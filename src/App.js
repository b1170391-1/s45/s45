import {Fragment} from "react";
import './App.css';

import {Container} from 'react-bootstrap'

/*components*/
import AppNavbar from './components/AppNavbar';

/*Pages*/
import Home from "./pages/Home" 
import Courses from "./pages/Courses"
import Register from "./pages/Register"
import Login from "./pages/Login"

function App() {

  return (
      <Fragment>
        <AppNavbar />
        <Container>
          <Home />
          <Courses />
          <Register />
          <Login />
        </Container>
      </Fragment>
  )

}

export default App;
