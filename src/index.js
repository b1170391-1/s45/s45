import React from 'react';
import ReactDOM from 'react-dom';

/*bootstrap stylesheet*/
import 'bootstrap/dist/css/bootstrap.min.css';

import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);


/*const name= "John Smith"
const element = <h1>Hello , {name}</h1>

ReactDOM.render (element, document.getElementById('root'));*/

/*const user = {
  firstName: "Jaden",
  lastName: "Smith"
}

function fullName(user){
  return `${user.firstName} ${user.lastName}`
}

const element = <h1>Hello, { fullName(user) }</h1>

ReactDOM.render(
  element, document.getElementById('root')

);
*/