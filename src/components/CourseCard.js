import {useState, useEffect} from 'react';
import {Row, Container, Col, Card, Button} from "react-bootstrap"
import PropTypes from 'prop-types';

export default function CourseCard({courseProp}) {
	console.log(courseProp)//object

	const {name, description, price} = courseProp

	const [count, setCount] = useState(0)
	const [bilang, setSeats] = useState(30)

	const [isDisabled, setIsDisabled] = useState(false)

	console.log(useState(0))
	console.log(useState(30))

	function enroll(){
		
		console.log(`Enrollees:` + count)
		if (count === 30 && bilang === 0)
		{
			alert("No more seats")
		}
		else
		{
			setCount(count + 1)
			setSeats (bilang - 1)
		}
	}

	useEffect( () => {

		if (bilang === 0){
			setIsDisabled(true)
		}
	}, [bilang])

	return (
	<Container fluid>
		<Row className="mt-4">
				<Col xs={10} md={8}>
					<Card className="cardHighlights p-4">
					<Card.Title>{name}</Card.Title>
					  <Card.Body>
					    <Card.Text>
					      Description:
					    </Card.Text>
					    <Card.Text>
					      {description}
					    </Card.Text>
					    <Card.Subtitle>
					    	Price:
					    </Card.Subtitle>
					    <Card.Text>
					    {price}
					    </Card.Text>
					    <Card.Text>Enrollees: {count}</Card.Text>

					    <Button variant="primary" onClick={enroll} disabled = {isDisabled}>

					    	Enroll

					    </Button>

					  </Card.Body>
					</Card>
				</Col>
		</Row>
	</Container>
	)
}

CourseCard.propTypes = {
	//shape method check if a prop object confirms to a specific shape
	courseProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}